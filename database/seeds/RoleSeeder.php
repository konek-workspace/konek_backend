<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleSeeder extends Seeder
{
    public function run()
    {
        DB::table('role')->delete();
        
        $json = File::get('database/data/role.json');
        $roles = json_decode($json);

        foreach ($roles as $role) 
        {
            Role::create(array(
                'id' => $role->id,
                'role_name' => $role->role_name,
                'role_desc' => $role->role_desc,
                'role_status' => $role->role_status,
                'created_at' => $role->created_at,
                'updated_at' => $role->updated_at,
            ));
        }
    }
}
