<?php

use Illuminate\Database\Seeder;
use App\QuantityUnit;

class QuantityUnitSeeder extends Seeder
{
    public function run()
    {
        DB::table('quantity_unit')->delete();
        
        $json = File::get('database/data/quantity_unit.json');
        $units = json_decode($json);

        foreach ($units as $unit) 
        {
            QuantityUnit::create(array(
                'id' => $unit->id,
                'unit_name' => $unit->unit_name,
                'created_at' => $unit->created_at,
                'updated_at' => $unit->updated_at
            ));
        }
    }
}
