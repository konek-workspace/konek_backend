<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            QuantityUnitSeeder::class,
            TimeUnitSeeder::class,
            BankSeeder::class,
            ProvinceSeeder::class,
            RegencySeeder::class,
            CommoditySeeder::class,
            RoleSeeder::class
        ]);
    }
}
