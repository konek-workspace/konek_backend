<?php

use Illuminate\Database\Seeder;
use App\TimeUnit;

class TimeUnitSeeder extends Seeder
{
    public function run()
    {
        DB::table('time_unit')->delete();
        
        $json = File::get('database/data/time_unit.json');
        $units = json_decode($json);

        foreach ($units as $unit) 
        {
            TimeUnit::create(array(
                'id' => $unit->id,
                'unit_name' => $unit->unit_name,
                'created_at' => $unit->created_at,
                'updated_at' => $unit->updated_at,
            ));
        }
    }
}
