<?php

use Illuminate\Database\Seeder;
use App\Bank;

class BankSeeder extends Seeder
{
    public function run()
    {
        DB::table('bank')->delete();
        
        $json = File::get('database/data/bank.json');
        $banks = json_decode($json);

        foreach ($banks as $bank) 
        {
            Bank::create(array(
                'id' => $bank->id,
                'bank_code' => $bank->bank_code,
                'bank_name' => $bank->bank_name,
                'created_at' => $bank->created_at,
                'updated_at' => $bank->updated_at
            ));
        }
    }
}
