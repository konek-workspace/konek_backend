<?php

use Illuminate\Database\Seeder;
use App\Regency;

class RegencySeeder extends Seeder
{
    public function run()
    {
        DB::table('regency')->delete();
        $json = File::get('database/data/regency.json');
        $data = json_decode($json);

        foreach ($data as $d) 
        {
            Regency::create(array(
                'id' => $d->id,
                'province_id' => $d->province_id,
                'regency_name' => $d->regency_name,
                'created_at' => $d->created_at,
                'updated_at' => $d->updated_at,
            ));
        }
    }
}
