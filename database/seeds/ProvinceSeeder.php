<?php

use Illuminate\Database\Seeder;
use App\Province;

class ProvinceSeeder extends Seeder
{
    public function run()
    {
        DB::table('province')->delete();
        $json = File::get('database/data/province.json');
        $data = json_decode($json);

        foreach ($data as $d) 
        {
            Province::create(array(
                'id' => $d->id,
                'province_name' => $d->province_name,
                'created_at' => $d->created_at,
                'updated_at' => $d->updated_at,
            ));
        }
    }
}
