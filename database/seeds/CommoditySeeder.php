<?php

use Illuminate\Database\Seeder;
use App\Commodity;
use App\SubCommodity;

class CommoditySeeder extends Seeder
{
    public function run()
    {
        try {
            DB::beginTransaction();

            DB::table('commodity')->delete();
            
            $json = File::get('database/data/commodity.json');
            $commodities = json_decode($json);

            foreach ($commodities as $commodity) 
            {
                Commodity::create(array(
                    'id' => $commodity->id,
                    'commodity_name' => $commodity->commodity_name,
                    'commodity_status' => $commodity->commodity_status,
                    'created_at' => $commodity->created_at,
                    'updated_at' => $commodity->updated_at,
                ));

                foreach ($commodity->sub_commodities as $sub_commodity)
                {
                    SubCommodity::create(array(
                        'commodity_id' => $commodity->id,
                        'sub_commodity_name' => $sub_commodity->sub_commodity_name,
                        'sub_commodity_desc' => $sub_commodity->sub_commodity_desc,
                        'sub_commodity_status' => $sub_commodity->sub_commodity_status,
                        'created_at' => $commodity->created_at,
                        'updated_at' => $commodity->updated_at,
                    ));
                }
            }

            DB::commit();
        }

        catch (\Exception $e) {
            DB::rollback();
            echo "Rolled back!!! ", $e->getMessage(), "\n";
        }
    }
}
