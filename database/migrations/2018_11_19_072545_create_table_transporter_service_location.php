<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTransporterServiceLocation extends Migration
{
    public function up()
    {
        Schema::create('transporter_service_location', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('transporter_desc_id')->unique();
            $table->integer('province_id');
            $table->timestamps();

            $table->foreign('transporter_desc_id')
                ->references('id')->on('transporter_desc')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('province_id')
                ->references('id')->on('province')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    public function down()
    {
        Schema::dropIfExists('transporter_service_location');
    }
}
