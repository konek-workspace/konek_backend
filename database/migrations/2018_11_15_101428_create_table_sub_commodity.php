<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSubCommodity extends Migration
{
    public function up()
    {
        Schema::create('sub_commodity', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sub_commodity_name')->unique();
            $table->string('sub_commodity_desc')->nullable();
            $table->smallInteger('sub_commodity_status')->nullable();
            $table->smallInteger('commodity_id');
            $table->timestamps();

            $table->foreign('commodity_id')
                ->references('id')->on('commodity')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    public function down()
    {
        Schema::dropIfExists('sub_commodity');
    }
}
