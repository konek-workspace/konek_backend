<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTransaction extends Migration
{
    public function up()
    {
        Schema::create('transaction', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sub_commodity_id');
            $table->integer('sender_id');
            $table->integer('receiver_id');
            $table->integer('transaction_quantity');
            $table->integer('quantity_unit_id');
            $table->double('transaction_value');
            $table->string('bank_account_holder');
            $table->smallInteger('bank_id');
            $table->timestamps();

            $table->foreign('sub_commodity_id')
                ->references('id')->on('sub_commodity')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('sender_id')
                ->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            
            $table->foreign('receiver_id')
                ->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('bank_id')
                ->references('id')->on('bank')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    public function down()
    {
        Schema::dropIfExists('transaction');
    }
}
