<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableRole extends Migration
{
    public function up()
    {
        Schema::create('role', function (Blueprint $table) {
            $table->smallInteger('id');
            $table->string('role_name')->unique();
            $table->string('role_desc')->nullable();
            $table->smallInteger('role_status')->nullable();
            $table->timestamps();

            $table->primary('id');
        });
    }

    public function down()
    {
        Schema::dropIfExists('role');
    }
}
