<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCompanyProfile extends Migration
{
    public function up()
    {
        Schema::create('company_profile', function (Blueprint $table) {
            $table->integer('user_id')->primary();
            $table->string('company_name');
            $table->string('company_address');
            $table->string('company_phone');
            $table->integer('company_regency_id');
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('company_regency_id')
                ->references('id')->on('regency')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    public function down()
    {
        Schema::dropIfExists('company_profile');
    }
}
