<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCommodityType extends Migration
{
    public function up()
    {
        Schema::create('commodity_type', function (Blueprint $table) {
            $table->increments('id');
            $table->string('commodity_type_name')->unique();
            $table->string('commodity_type_status')->nullable();
            $table->integer('sub_commodity_id');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('commodity_type');
    }
}
