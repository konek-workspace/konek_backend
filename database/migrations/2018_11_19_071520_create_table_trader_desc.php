<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTraderDesc extends Migration
{
    public function up()
    {
        Schema::create('trader_desc', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_role_id')->unique();
            $table->smallInteger('season_period')
                ->comment('1 means trade same quantity year-around. if 0, begin and end of season are required');
            $table->smallInteger('season_begin')->nullable()->comment('1: January, 12: December');
            $table->smallInteger('season_end')->nullable()->comment('1: January, 12: December');;
            $table->integer('seasonal_throughput');
            $table->integer('seasonal_throughput_quantity_unit_id');
            $table->integer('seasonal_throughput_time_unit_id');
            $table->timestamps();

            $table->foreign('company_role_id')
                ->references('id')->on('company_role')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('seasonal_throughput_quantity_unit_id')
                ->references('id')->on('quantity_unit')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('seasonal_throughput_time_unit_id')
                ->references('id')->on('time_unit')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    public function down()
    {
        Schema::dropIfExists('trader_desc');
    }
}
