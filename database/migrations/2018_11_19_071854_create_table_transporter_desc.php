<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTransporterDesc extends Migration
{
    public function up()
    {
        Schema::create('transporter_desc', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_role_id')->unique();
            $table->smallInteger('transportation_type')->comment('1: Land; 2: Sea; 3: Air;');
            $table->smallInteger('service_location')->nullable()
                ->comment('1: all around Indonesia');
            $table->timestamps();

            $table->foreign('company_role_id')
                ->references('id')->on('company_role')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    public function down()
    {
        Schema::dropIfExists('transporter_desc');
    }
}
