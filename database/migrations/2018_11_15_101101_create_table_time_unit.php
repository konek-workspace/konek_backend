<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTimeUnit extends Migration
{
    public function up()
    {
        Schema::create('time_unit', function (Blueprint $table) {
            $table->smallInteger('id');
            $table->string('unit_name')->unique();
            $table->timestamps();

            $table->primary('id');
        });
    }

    public function down()
    {
        Schema::dropIfExists('time_unit');
    }
}
