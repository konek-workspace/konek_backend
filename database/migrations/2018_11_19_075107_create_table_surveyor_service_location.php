<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSurveyorServiceLocation extends Migration
{
    public function up()
    {
        Schema::create('surveyor_service_location', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('surveyor_desc_id');
            $table->integer('province_id');
            $table->timestamps();

            $table->foreign('surveyor_desc_id')
                ->references('id')->on('surveyor_desc')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('province_id')
                ->references('id')->on('province')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    public function down()
    {
        Schema::dropIfExists('surveyor_service_location');
    }
}
