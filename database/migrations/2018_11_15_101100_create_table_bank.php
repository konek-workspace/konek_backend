<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableBank extends Migration
{
    public function up()
    {
        Schema::create('bank', function (Blueprint $table) {
            $table->smallInteger('id');
            $table->string('bank_code', 5)->unique();
            $table->string('bank_name');
            $table->timestamps();

            $table->primary('id');
        });
    }

    public function down()
    {
        Schema::dropIfExists('bank');
    }
}
