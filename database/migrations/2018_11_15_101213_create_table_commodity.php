<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCommodity extends Migration
{
    public function up()
    {
        Schema::create('commodity', function (Blueprint $table) {
            $table->smallInteger('id');
            $table->string('commodity_name')->unique();
            $table->smallInteger('commodity_status')->nullable();
            $table->timestamps();

            $table->primary('id');
        });
    }

    public function down()
    {
        Schema::dropIfExists('commodity');
    }
}
