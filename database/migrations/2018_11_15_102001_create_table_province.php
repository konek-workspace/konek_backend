<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableProvince extends Migration
{
    public function up()
    {
        Schema::create('province', function (Blueprint $table) {
            $table->integer('id')->primary();
            $table->string('province_name');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('province');
    }
}
