<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCompanyRole extends Migration
{
    public function up()
    {
        Schema::create('company_role', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('sub_commodity_id');
            $table->string('commodity_type');
            $table->integer('role_id');
            $table->timestamps();

            $table->unique(['user_id', 'sub_commodity_id', 'role_id']);

            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('sub_commodity_id')
                ->references('id')->on('sub_commodity')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('role_id')
                ->references('id')->on('role')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    public function down()
    {
        Schema::dropIfExists('company_role');
    }
}
