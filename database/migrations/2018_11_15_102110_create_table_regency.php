<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableRegency extends Migration
{
    public function up()
    {
        Schema::create('regency', function (Blueprint $table) {
            $table->integer('id')->primary();
            $table->string('regency_name');
            $table->integer('province_id');
            $table->timestamps();

            $table->foreign('province_id')->references('id')->on('province');
        });
    }

    public function down()
    {
        Schema::dropIfExists('regency');
    }
}
