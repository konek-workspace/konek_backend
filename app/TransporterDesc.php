<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransporterDesc extends Model
{
    protected $table = 'transporter_desc';
    protected $hidden = ['created_at', 'updated_at'];
    
    public function companyRole()
    {
        return $this->belongsTo('App\CompanyRole');
    }

    public function serviceLocations()
    {
        return $this->hasMany('App\TransporterServiceLocation');
    }
}
