<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostComment extends Model
{
    protected $table = 'post_comment';
    protected $hidden = ['created_at', 'updated_at'];

    public function post()
    {
        return $this->belongsTo('App\Post');
    }
}
