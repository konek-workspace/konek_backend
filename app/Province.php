<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    protected $table = 'province';
    protected $hidden = ['created_at', 'updated_at'];

    public function regencies() {
        return $this->hasMany('App\Regency');
    }
}