<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuantityUnit extends Model
{
    protected $table = 'quantity_unit';
    protected $hidden = ['created_at', 'updated_at'];

    // public const KILOGRAM = 1;
    // public const TON      = 2;
}