<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TimeUnit extends Model
{
    protected $table = 'time_unit';
    protected $hidden = ['created_at', 'updated_at'];

    // public const WEEK   = 1;
    // public const MONTH  = 2;
    // public const YEAR   = 3;
    // public const UNITS  = array(WEEK, MONTH, YEAR);

    // public const JAN = 1;
    // public const FEB = 2;
    // public const MAR = 3;
    // public const APR = 4;
    // public const MAY = 5;
    // public const JUN = 6;
    // public const JUL = 7;
    // public const AUG = 8;
    // public const SEP = 9;
    // public const OCT = 10;
    // public const NOV = 11;
    // public const DEC = 12;
}
