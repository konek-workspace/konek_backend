<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SurveyorDesc extends Model
{
    protected $table = 'surveyor_desc';
    protected $hidden = ['created_at', 'updated_at'];

    public function companyRole()
    {
        return $this->belongsTo('App\CompanyRole');
    }

    public function serviceLocations()
    {
        return $this->hasMany('App\SurveyorServiceLocation');
    }
}