<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransporterServiceLocation extends Model
{
    protected $table = 'transporter_service_location';
    protected $fillable = ['province_id'];
    protected $hidden = ['created_at', 'updated_at'];
    
    public function transporterDesc()
    {
        return $this->belongsTo('App\TransporterDesc');
    }
}