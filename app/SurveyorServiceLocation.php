<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SurveyorServiceLocation extends Model
{
    protected $table = 'surveyor_service_location';
    protected $fillable = ['province_id'];
    protected $hidden = ['created_at', 'updated_at'];
    
    public function surveyorDesc()
    {
        return $this->belongsTo('App\SurveyorDesc');
    }
}
