<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubCommodity extends Model
{
    protected $table = 'sub_commodity';
    protected $hidden = ['created_at', 'updated_at'];

    public function commodity()
    {
        return $this->belongsTo('App\Commodity');
    }
    
    public function userProfile()
    {
        return $this->hasMany('App\UserProfile', 'user_id', 'user_id');
    }

    public function companyProfile()
    {
        return $this->hasMany('App\CompanyProfile', 'user_id', 'user_id');
    }
}