<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ThirdParty extends Model
{
    protected $table = 'third_party';
    protected $hidden = ['created_at', 'updated_at'];

    public function transaction()
    {
        return $this->belongsTo('transaction');
    }

    public function user()
    {
        return $this->belongsTo('users');
    }

    public function userProfile()
    {
        return $this->belongsTo('user_profile','user_id','user_id');
    }

    public function companyProfile()
    {
        return $this->belongsTo('company_profile', 'user_id', 'user_id');
    }
}
