<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Regency extends Model
{
    protected $table = 'regency';
    protected $hidden = ['created_at', 'updated_at'];

    public function province() {
        return $this->belongsTo('App\Province');
    }
}
