<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyProfile extends Model
{
    protected $table = 'company_profile';
    protected $primaryKey = 'user_id';
    protected $hidden = ['created_at', 'updated_at'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function userProfile()
    {
        return $this->belongsTo('App\UserProfile', 'user_id', 'user_id');
    }

    public function companyRoles()
    {
        return $this->hasMany('App\CompanyRole', 'user_id', 'user_id');
    }

    public function regency()
    {
        return $this->belongsTo('App\Regency', 'company_regency_id');
    }

}
