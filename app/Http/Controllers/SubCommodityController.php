<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SubCommodity;

class SubCommodityController extends Controller
{
    public function index()
    {
        $subCommodities = SubCommodity::all();

        return response()->json([
            'status' => 200,
            'sub_commodities' => $subCommodities
        ]);
    }

    public function store(Request $request)
    { 
        $subCommodity = new SubCommodity;

        $subCommodity->sub_commodity_name = $request->input('sub_commodity_name');
        $subCommodity->sub_commodity_desc = $request->input('sub_commodity_desc');
        $subCommodity->sub_commodity_status = $request->input('sub_commodity_status');
        $subCommodity->commodity_id = $request->input('commodity_id');

        $subCommodity->save();

        return response()->json([
            'status' => 201,
            'sub_commoditiy' => $subCommodity
        ]);
    }

    public function show($id)
    {
        $subCommodity = SubCommodity::find($id);
        
        return response()->json([
            'status' => 200,
            'sub_commodity' => $subCommodity
        ]);
    }

    public function update(Request $request, $id)
    { 
        $subCommodity = SubCommodity::find($id);

        $subCommodity->sub_commodity_name = $request->input('sub_commodity_name');
        $subCommodity->sub_commodity_desc = $request->input('sub_commodity_desc');
        $subCommodity->sub_commodity_status = $request->input('sub_commodity_status');
        $subCommodity->commodity_id = $request->input('commodity_id');

        $subCommodity->save();

        return response()->json([
            'status' => 201,
            'sub_commoditiy' => $subCommodity
        ]);
    }

    public function destroy($id)
    { 
        $subCommodity = SubCommodity::find($id);

        $subCommodity->delete();

        return response()->json([
            'status' => 201,
            'message' => 'delete success'
        ]);
    }

    // filter commodity by it's sub commodity, province and role
    public function filter($subCommodityId, $provinceId, $roleId)
    {
        return CommodityController::filterByRole($subCommodityId, $provinceId, $roleId);
    }

}
