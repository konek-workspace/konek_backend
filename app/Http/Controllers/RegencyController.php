<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Regency;

class RegencyController extends Controller
{
    public function index()
    {
        $regency = Regency::all();

        return response()->json([
            'status' => 200,
            'regencies' => $regency]);
    }

    public function store(Request $request)
    {
        $regency = new Regency;

        $regency->id = $request->input('id');
        $regency->regency_name = $request->input('regency_name');
        $regency->province_id = $request->input('province_id');

        $regency->save();

        return response()->json([
            'status' => 201,
            'regency' => $regency
        ]);
    }

    public function show($id)
    {
        $regency = Regency::find($id);
        
        return response()->json([
            'status' => 200,
            'regency' => $regency
        ]);
    }

    public function update(Request $request, $id)
    {
        $regency = Regency::find($id);

        $regency->id = $request->input('id');
        $regency->regency_name = $request->input('regency_name');
        $regency->province_id = $request->input('province_id');

        $regency->save();

        return response()->json([
            'status' => 201,
            'regency' => $regency
        ]);
    }

    public function destroy($id)
    {
        $regency = Regency::find($id);

        $regency->delete();

        return response()->json([
            'status' => 201,
            'message' => 'delete success'
        ]);
    }
}