<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transaction;

class TransactionController extends Controller
{
    public function index()
    {
        $transactions = Transaction::all();

        return response()->json([
            'status' => 200,
            'transactions' => $transactions
        ]);
    }

    public function store(Request $request)
    {
        $transaction = new Transaction;

        $transaction->sub_commodity_id = $request->input('sub_commodity_id');
        $transaction->sender_id = $request->input('sender_id');
        $transaction->receiver_id = $request->input('receiver_id');
        $transaction->transaction_quantity = $request->input('transaction_quantity');
        $transaction->quantity_unit_id = $request->input('quantity_unit_id');
        $transaction->transaction_value = $request->input('transaction_value');
        $transaction->bank_account_holder = $request->input('bank_account_holder');
        $transaction->bank_id = $request->input('bank_id');

        $transaction->save();

        return response()->json([
            'status' => 201,
            'transaction' => $transaction
        ]);
    }

    public function show($id)
    {
        $transaction = Transaction::find($id);
        return response()->json([
            'status' => 200,
            'transaction' => $transaction
        ]);
    }

    public function update(Request $request, $id)
    {
        $transaction = Transaction::find($id);

        $transaction->sub_commodity_id = $request->input('sub_commodity_id');
        $transaction->sender_id = $request->input('sender_id');
        $transaction->receiver_id = $request->input('receiver_id');
        $transaction->transaction_quantity = $request->input('transaction_quantity');
        $transaction->quantity_unit_id = $request->input('quantity_unit_id');
        $transaction->transaction_value = $request->input('transaction_value');
        $transaction->bank_account_holder = $request->input('bank_account_holder');
        $transaction->bank_id = $request->input('bank_id');

        $transaction->save();

        return response()->json([
            'status' => 201,
            'transaction' => $transaction
        ]);
    }

    public function destroy($id)
    {
        $transaction = Transaction::find($id);

        $transaction->delete();

        return response()->json([
            'status' => 201,
            'message' => 'delete success'
        ]);
    }

    // get user transactions
    public function filterByUserId($userId)
    {
        $transactions = Transaction::with('sender')->
            with('receiver')->
            whereRaw('sender_id = ? or receiver_id = ?', [$userId, $userId])->
            find();

        return response()->json([
            'status' => 200,
            'transaction' => $transactions
        ]);
    }
}
