<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Regency;

class RegencyLookup extends Controller
{
    public function filterByProvinceId($id)
    {
        $regency = Regency::where('province_id', $id)->get();
        return response()->json([
            'status' => 200,
            'regencies' => $regency
        ]);
    }
}