<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CompanyRole;

class CompanyRoleController extends Controller
{
    public function index()
    { 
        $companyRoles = CompanyRole::all();

        return response()->json([
            'status' => 200,
            'company_roles' => $companyRoles
        ]);
    }

    public function store(Request $request)
    { 
        $companyRole = new CompanyRole;

        $companyRole->user_id = $request->input('user_id');
        $companyRole->sub_commodity_id = $request->input('sub_commodity_id');
        $companyRole->commodity_type = $request->input('commodity_type');
        $companyRole->role_id = $request->input('role_id');

        $companyRole->save();

        return response()->json([
            'status' => 201,
            'company_role' => $companyRole
        ]);
    }

    public function show($id)
    {
        $companyRole = CompanyRole::find($id);

        return response()->json([
            'status' => 200,
            'company_role' => $companyRole
        ]);
    }

    public function update(Request $request, $id)
    {
        $companyRole = CompanyRole::find($id);

        $companyRole->user_id = $request->input('user_id');
        $companyRole->sub_commodity_id = $request->input('sub_commodity_id');
        $companyRole->commodity_type = $request->input('commodity_type');
        $companyRole->role_id = $request->input('role_id');

        $companyRole->save();

        return response()->json([
            'status' => 201,
            'company_role' => $companyRole
        ]);
    }

    public function destroy($id)
    {
        $companyRole = CompanyRole::find($id);

        $companyRole->delete();

        return response()->json([
            'status' => 200,
            'message' => 'delete success'
        ]);
    }

    // filter commodity by it's sub commodity, province and role
    public function filter($subCommodityId, $provinceId, $roleId)
    {
        return CommodityController::filterByRole($subCommodityId, $provinceId, $roleId);
    }
}
