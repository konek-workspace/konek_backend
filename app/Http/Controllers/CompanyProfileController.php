<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CompanyProfile;

class CompanyProfileController extends Controller
{
    public function index()
    { 
        $companies = CompanyProfile::with('userProfile')->get();

        return response()->json([
            "status" => 200,
            "companies" => $companies
        ]);
    }

    public function store(Request $request)
    { 
        $companyProfile = new CompanyProfile;
        
        $companyProfile->user_id = $request->input('user_id');
        $companyProfile->company_name = $request->input('company_name');
        $companyProfile->company_address = $request->input('company_address');
        $companyProfile->company_phone = $request->input('company_phone');
        $companyProfile->company_regency_id = $request->input('company_regency_id');

        $companyProfile->save();

        return response()->json([
            "status" => 201,
            "company_profile" => $companyProfile
        ]);
    }

    public function show($id)
    { 
        $company = CompanyProfile::with([
                'userProfile',
                'companyRoles.producerDesc', 
                'companyRoles.traderDesc',
                'companyRoles.transporterDesc',
                'companyRoles.surveyorDesc',
                'companyRoles.purchaserDesc'
            ])->where('user_id', $id)->first();

        return response()->json([
            "status" => 200,
            "company" => $company
        ]);
    }

    public function update(Request $request, $id)
    { 
        $companyProfile = CompanyProfile::find($id);
        
        $companyProfile->user_id = $request->input('user_id');
        $companyProfile->company_name = $request->input('company_name');
        $companyProfile->company_address = $request->input('company_address');
        $companyProfile->company_phone = $request->input('company_phone');
        $companyProfile->company_regency_id = $request->input('company_regency_id');

        $companyProfile->save();

        return response()->json([
            "status" => 201,
            "company_profile" => $companyProfile
        ]);
    }

    public function destroy($id)
    { 
        $companyProfile = CompanyProfile::find($id);
        
        $companyProfile->delete();

        return response()->json([
            "status" => 201,
            "message" => 'delete success'
        ]);
    }
}
