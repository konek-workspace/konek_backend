<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\PostTag;
use DB;

class PostController extends Controller
{
    public function index()
    { 
        $posts = Post::all();
        
        return response()->json([
            'status' => 200,
            'posts' => $posts
        ]);
    }

    public function store(Request $request)
    { 
        $post = new Post();
        
        $post->user_id = $request->input('user_id');
        $post->sub_commodity_id = $request->input('sub_commodity_id');
        $post->title = $request->input('title');
        $post->post_body = $request->input('post_body');

        $post->save();

        $tags = preg_split("/[;,]+/", $request->input('tags'));

        // save only when tag more than zero
        if (sizeof($tags) > 0)
        {
            $postTags = array();
            foreach ($tags as $tag)
            {
                // ignore null char tag 
                if (strlen($tag) > 1) 
                {
                    array_push($postTags, new PostTag(['tag_name' => $tag]));
                }
            }
            $post->tags()->saveMany($postTags);
        }

        return response()->json([
            'status' => 201,
            'post' => $post
        ]);
    }

    public function show($id)
    {
        $post = Post::find($id);
        
        return response()->json([
            'status' => 200,
            'post' => $post
        ]);
    }

    public function update(Request $request, $id)
    { 
        $post = Post::find($id);
        
        $post->sub_commodity_id = $request->input('sub_commodity_id');
        $post->title = $request->input('title');
        $post->post_body = $request->input('post_body');

        $post->save();

        return response()->json([
            'status' => 201,
            'post' => $post
        ]);
    }

    public function destroy($id)
    {
        $post = Post::find($id);

        $post->delete();
        
        return response()->json([
            'status' => 201,
            'message' => 'delete success'
        ]);
    }

    public function getPostBySubCommodity($subCommodityId)
    {
        $posts = Post::where('sub_commodity_id', $subCommodityId)
            ->with('user')
            ->with('tags')->with('comments')
            ->get();
        
        return response()->json([
            'status' => 200,
            'posts' => $posts
        ]);   
    }
}
