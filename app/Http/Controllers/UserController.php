<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Http\Requests\UserStoreRequest;
use App\User;
use App\UserProfile;
use App\CompanyProfile;
use App\CompanyRole;
use App\ProducerDesc;
use App\TraderDesc;
use App\TransporterDesc;
use App\TransporterServiceLocation;
use App\SurveyorDesc;
use App\SurveyorServiceLocation;
use App\PurchaserDesc;
use App\Role;
use DB;
class UserController extends Controller
{
    use AuthenticatesUsers;

    public function index()
    {
        $users = User::all();
        return response()->json([
            'status' => 200,
            'users' => $users
        ]);
    }

    public function store(UserStoreRequest $request)
    {
        // request body must contains these fields
        // name, email, password (required in users table)
        // nik, fullname, user_address, user_regency_id (required in users_profiles table)
        // company_name, company_address, company_regency_id (required in companies_profiles)
        // sub_commodity_id, commodity_type, role_id (required in companies_roles. required min 1)

        $user = new User();

        try {
            DB::beginTransaction();

            // insert into users
            $user = new User();
            $user->name     = $request->input('name');
            $user->email    = $request->input('email');
            $user->password = Hash::make($request->input('password'));
            $user->save();

            // insert into user_profile
            $userProfile = new UserProfile();
            $userProfile->nik             = $request->input('nik');
            $userProfile->fullname        = $request->input('fullname');
            $userProfile->user_address    = $request->input('user_address');
            $userProfile->user_regency_id = $request->input('user_regency_id');
            $user->userProfile()->save($userProfile);

            // insert into company_profile
            $companyProfile = new CompanyProfile();
            $companyProfile->company_name       = $request->input('company_name');
            $companyProfile->company_address    = $request->input('company_address');
            $companyProfile->company_phone    = $request->input('company_phone');
            $companyProfile->company_regency_id = $request->input('company_regency_id');
            $user->companyProfile()->save($companyProfile);

            // get all selected roles;
            $roles = $request->input('company_roles');
            
            foreach ($roles as $role) 
            {
                $companyRole = new CompanyRole();
                $companyRole->sub_commodity_id = $role['sub_commodity_id'];
                $companyRole->commodity_type   = $role['commodity_type'];
                $companyRole->role_id          = $role['role_id'];
                $user->companyRoles()->save($companyRole);

                switch($role['role_id']) {

                    // when rule is producer (produsen, 1)
                    case Role::PRODUCER: 
                        $producer = $this->saveDesc(Role::PRODUCER, new ProducerDesc, $role['description']);
                        $companyRole->producerDesc()->save($producer);
                        break;

                    // when rule is trader (pedagang, 2)
                    case Role::TRADER: 
                        $trader = $this->saveDesc(Role::TRADER, new TraderDesc, $role['description']);
                        $companyRole->traderDesc()->save($trader);
                        break;

                    // when rule is transporter (penyedia angkutan, 3)
                    case Role::TRANSPORTER: 
                        $transporter = $this->saveDesc(Role::TRANSPORTER, new TransporterDesc, $role['description']);
                        $companyRole->transporterDesc()->save($transporter);
                        
                        // transport to several provinces
                        if (!$transporter->service_location)
                        {
                            $provinceIds = $role['description']['service_locations'];
                            $serviceLocations = array();

                            foreach ($provinceIds as $provinceId)
                            {
                                array_push($serviceLocations, 
                                    new TransporterServiceLocation([
                                        'province_id' => $provinceId
                                    ])
                                );
                            }

                            $transporter->serviceLocations()->saveMany($serviceLocations);                         
                        }
                        break;

                    // when rule is surveyor (inspektur, 4)
                    case Role::SURVEYOR: 
                        $surveyor = $this->saveDesc(Role::SURVEYOR, new SurveyorDesc, $role['description']);
                        $companyRole->surveyorDesc()->save($surveyor);
                        
                        // survey to several provinces
                        if (!$surveyor->service_location)
                        {
                            $provinceIds = $role['description']['service_locations'];
                            $serviceLocations = array();

                            foreach ($provinceIds as $provinceId)
                            {
                                array_push($serviceLocations, 
                                    new SurveyorServiceLocation([
                                        'province_id' => $provinceId
                                    ])
                                );
                            }

                            $surveyor->serviceLocations()->saveMany($serviceLocations);                         
                        }
                        break;

                    // when rule is purchaser (industri/pembeli, 5)
                    case Role::PURCHASER: 
                        $purchaser = $this->saveDesc(Role::PURCHASER, new PurchaserDesc, $role['description']);
                        $companyRole->purchaserDesc()->save($purchaser);
                        break;
                }
            }

            $token = $user->createToken('create_user')->accessToken;

            DB::commit();
        }
        
        catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'status' => 500,
                'message' => $e->getMessage()]);
        }

        return response()->json([
            'status' => 201,
            'user' => [
                'id' => $user->id,
                'name' => $user->name,
                'email' => $user->email
            ],
            'token' => $token, 
            'message' => 'User created.'
        ]);
    }

    public function show($id)
    {
        $user = User::
            with('userProfile.regency.province')->
            with('companyProfile.regency.province')->
            with('companyRoles.producerDesc')->
            with('companyRoles.transporterDesc')->
            find($id);

        return response()->json([
            'status' => 200,
            'user' => $user
        ]);
    }

    public function update(Request $request, $id)
    { }

    public function destroy($id)
    { 
        $user = User::find($id);

        $user->delete();

        return response()->json([
            'status' => 201,
            'message' => 'delete success'
        ]);
    }

    // login for API
    public function login(Request $request)
    {
        try 
        {
            if (Auth::attempt(['email' => $request->input('email'), 
            'password' => $request->input('password')]))
            { 
                $user = Auth::user(); 
                $token =  $user->createToken('konek')->accessToken; 
                
                return response()->json([
                    'status' => 200,
                    'user' => $user,
                    'token' => $token   
                ]); 
            } 
            
            else
            { 
                return response()->json([
                    'status' => 401,
                    'message' => 'Unauthenticated']
                ); 
            }
        }
        
        catch (\Exception $e)
        {
            return response()->json([
                'status' => 500,
                'message'=> $e->getMessage()
            ]); 
        }
    }

    private function saveDesc($roleId, $roleObj, $roleDesc)
    {
        // only if role_id is one of the following: producer(1), trader(2), purchaser(5) 
        if (in_array($roleId, array(Role::PRODUCER, Role::TRADER, Role::PURCHASER)))
        {
            $roleObj->season_period                        = $roleDesc['season_period'];
            $roleObj->season_begin                         = $roleDesc['season_begin'];
            $roleObj->season_end                           = $roleDesc['season_end'];
            $roleObj->seasonal_throughput                  = $roleDesc['seasonal_throughput'];
            $roleObj->seasonal_throughput_quantity_unit_id = $roleDesc['seasonal_throughput_quantity_unit_id'];
            $roleObj->seasonal_throughput_time_unit_id     = $roleDesc['seasonal_throughput_time_unit_id'];
        }

        // only if role_id is transporter(3)
        else if ($roleId == Role::TRANSPORTER)
        {
            // transport to some province
            if (is_array($roleDesc['service_locations']))
            {
                $roleObj->transportation_type = $roleDesc['transportation_type'];
                $roleObj->service_location    = 0;
                // save appropriate service location(s) after returning value
            }

            // transport all-around Indonesia
            else
            {
                $roleObj->transportation_type = $roleDesc['transportation_type'];
                $roleObj->service_location    = 1;
            }
        }

        // only if role_id is surveyor(4)
        else if ($roleId == Role::SURVEYOR)
        {
            // survey to some provinces
            if (is_array($roleDesc['service_locations']))
            {
                $roleObj->service_location = 0;
                // save appropriate service location(s) after returning value
            }

            // survey all-around Indonesia
            else
            {
                $roleObj->service_location = 1;
            }
        }

        return $roleObj;
    }

    public function getContacts($id)
    {
        $query = "SELECT 
            ct.contact_id,
            usr.name,
            up.fullname,
            cp.company_name

            FROM contact ct
            JOIN users usr ON usr.id = ct.contact_id
            JOIN user_profile up ON up.user_id = ct.contact_id
            JOIN company_profile cp ON cp.user_id = ct.contact_id  
            WHERE ct.user_id = $id";

        $contacts = DB::select($query);

        return response()->json([
            "status" => 200,
            "contacts" => $contacts
        ]);
    }

    public function getProfile($id)
    {
        $query = "SELECT
            usr.id,
            usr.name,
            up.fullname,
            cp.company_name,
            cp.company_phone,
            cp.company_address,
            rg.regency_name,
            pv.province_name,
            TO_CHAR(usr.created_at, 'MONTH YYYY') created_at
            FROM users usr
            JOIN user_profile up ON up.user_id = usr.id
            JOIN company_profile cp ON cp.user_id = usr.id
            JOIN regency rg ON rg.id = cp.company_regency_id
            JOIN province pv ON pv.id = rg.province_id
            WHERE usr.id = :id";

        $profile = DB::select($query, ['id' => $id]);
        $profile = collect($profile)->first();

        return response()->json([
            "status" => 200,
            "profile" => $profile
        ]);
    }

}
