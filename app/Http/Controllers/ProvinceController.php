<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Province;

class ProvinceController extends Controller
{
    public function index()
    {
        $provinces = Province::all();
        
        return response()->json([
            'status' => 200,
            'provinces' => $provinces
        ]);
    }

    public function store(Request $request)
    { 
        $province = new Province;

        $province->id = $request->input('id');
        $province->province_name = $request->input('province_name');

        $province->save();

        return response()->json([
            'status' => 201,
            'province' => $province
        ]);
    }

    public function show($id)
    {
        $province = Province::with('regencies')->find($id);
        
        return response()->json([
            'status' => 200,
            'province' => $province
        ]);
    }

    public function update(Request $request, $id)
    { 
        $province = Province::find($id);

        $province->id = $request->input('id');
        $province->province_name = $request->input('province_name');

        $province->save();

        return response()->json([
            'status' => 201,
            'province' => $province
        ]);
    }

    public function destroy($id)
    { 
        $province = Province::find($id);

        $province->delete();

        return response()->json([
            'status' => 201,
            'message' => 'delete success'
        ]);
    }
}