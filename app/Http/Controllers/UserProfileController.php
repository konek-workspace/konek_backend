<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserProfile;

class UserProfileController extends Controller
{
    public function index()
    { 
        $userProfile = UserProfile::with('companyProfile')->get();

        return response()->json([
            "status" => 200,
            "user_profile" => $userProfile
        ]);
    }

    public function store(Request $request)
    { 
        $userProfile = new UserProfile;

        $userProfile->user_id = $request->input('user_id');
        $userProfile->nik = $request->input('nik');
        $userProfile->fullname = $request->input('fullname');
        $userProfile->user_address = $request->input('user_address');
        $userProfile->user_regency_id = $request->input('user_regency_id');
        
        $userProfile->save();

        return response()->json([
            "status" => 201,
            "user_profile" => $userProfile
        ]); 
    }

    public function show($id)
    {
        $userProfile = UserProfile::with('companyProfile')->
            with(['companyRoles.producerDesc', 
                'companyRoles.traderDesc',
                'companyRoles.transporterDesc',
                'companyRoles.surveyorDesc',
                'companyRoles.purchaserDesc'])->
            where('user_id', $id)->first();

        return response()->json([
            "status" => 200,
            "user_profile" => $userProfile
        ]);
    }

    public function update(Request $request, $id)
    {
        $userProfile = UserProfile::where('user_id', $id)->first();

        $userProfile->nik = $request->input('nik');
        $userProfile->fullname = $request->input('fullname');
        $userProfile->user_address = $request->input('user_address');
        $userProfile->user_regency_id = $request->input('user_regency_id');
        
        $userProfile->save();

        return response()->json([
            "status" => 201,
            "user_profile" => $userProfile
        ]);
    }

    public function destroy($id)
    { 
        $userProfile = UserProfile::where('user_id', $id)->first();

        $userProfile->delete();
        
        return response()->json([
            "status" => 201,
            "message" => "delete success"
        ]);
    }
}
