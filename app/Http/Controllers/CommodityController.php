<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Commodity;
use App\SubCommodity;
use App\Role;

class CommodityController extends Controller
{
    public function index()
    {
        $commodities = Commodity::all();

        return response()->json([
            'status' => 200,
            'commodities' => $commodities
        ]);
    }

    public function store(Request $request)
    { 
        $commodity = new Commodity;

        $commodity->commodity_name = $request->input('commodity_name');
        $commodity->commodity_status = $request->input('commodity_status');
        
        return response()->json([
            'status' => 201,
            'commodity' => $commodity
        ]);
    }

    public function show($id)
    {
        $commodity = Commodity::with('subCommodities')->find($id);
        
        return response()->json([
            'status' => 200,
            'commodity' => $commodity
        ]);
    }

    public function update(Request $request, $id)
    { 
        $commodity = Commodity::find($id);

        $commodity->commodity_name = $request->input('commodity_name');
        $commodity->commodity_status = $request->input('commodity_status');
        
        return response()->json([
            'status' => 201,
            'commodity' => $commodity
        ]);
    }

    public function destroy($id)
    { 
        $commodity = Commodity::find($id);

        $commodity->delete();

        return response()->json([
            'status' => 201,
            'message' => 'delete success'
        ]);
    }

    public function filter($subCommodityId, $provinceId, $roleId)
    {
        return CommodityController::filterByRole($subCommodityId, $provinceId, $roleId);
    }

    public function getSubCommodity($id) {
        $commodity = Commodity::with('subCommodities')->find($id);
        
        return response()->json([
            'status' => 200,
            'commodity' => $commodity
        ]);
    }
    
    // filter commodity by it's sub commodity, province and role
    public static function filterByRole($subCommodityId, $provinceId, $roleId)
    {
        switch ($roleId)
        {
            // filter producer
            case 1: 
                $commodities = CommodityController::filterProducer($subCommodityId, $provinceId);
                break;
            
            // filter trader
            case 2:
                $commodities = CommodityController::filterTrader($subCommodityId, $provinceId);
                break;

            // filter transporter
            case 3:
                $commodities = CommodityController::filterTransporter($subCommodityId, $provinceId);
                break;

            // filter surveyor 
            case 4:
                $commodities = CommodityController::filterSurveyor($subCommodityId, $provinceId);
                break;

            // filter purchaser
            case 5:
                $commodities = CommodityController::filterPurchaser($subCommodityId, $provinceId);
                break;
        }

        return response()->json([
            'status' => 200,
            'commodities' => $commodities]);
    }

    // get commodity list
    // @params: commodity_id, user_id (login user)
    public function companies($id, $userId)
    {
        $query = "SELECT 
            cp.user_id,
            sc.commodity_id,
            cp.company_name,
            up.fullname,
            rg.regency_name,
            pv.province_name,
            rl.role_name,
            cdy.commodity_name,
            sc.sub_commodity_name,
            cr.commodity_type,
            COALESCE(ct.contact_id, 0) is_contact

            FROM company_role cr
            JOIN role rl ON rl.id = cr.role_id
            JOIN sub_commodity sc ON sc.id = cr.sub_commodity_id
            JOIN user_profile up ON up.user_id = cr.user_id
            JOIN company_profile cp ON cp.user_id = cr.user_id
            JOIN regency rg ON rg.id = cp.company_regency_id
            JOIN province pv ON pv.id = rg.province_id
            JOIN commodity cdy ON cdy.id = sc.commodity_id
            LEFT JOIN contact ct ON ct.user_id = $userId AND ct.contact_id = cp.user_id
            WHERE sc.commodity_id = $id";

        $companies = \DB::select($query);

        return response()->json([
            "status" => 200,
            "companies" => $companies
        ]);
    }

    private static function filterProducer($subCommodityId, $provinceId)
    {
        $query = "SELECT 
            cp.user_id,
            sc.commodity_id,
            cp.company_name,
            up.fullname,
            rg.regency_name,
            pv.province_name,
            rl.role_name,
            cdy.commodity_name,
            sc.sub_commodity_name,
            cr.commodity_type,
            1 is_contact

            FROM company_role cr
            JOIN role rl ON rl.id = cr.role_id
            JOIN sub_commodity sc ON sc.id = cr.sub_commodity_id
            JOIN user_profile up ON up.user_id = cr.user_id
            JOIN company_profile cp ON cp.user_id = cr.user_id
            JOIN regency rg ON rg.id = cp.company_regency_id
            JOIN province pv ON pv.id = rg.province_id
            JOIN commodity cdy ON cdy.id = sc.commodity_id
            WHERE cr.sub_commodity_id = $subCommodityId
            AND pv.id = $provinceId
            AND cr.role_id = ".Role::PRODUCER;

        return \DB::select($query);
    }

    private static function filterTrader($subCommodityId, $provinceId)
    {
        $query = "SELECT 
            cp.user_id,
            sc.commodity_id,
            cp.company_name,
            up.fullname,
            rg.regency_name,
            pv.province_name,
            rl.role_name,
            cdy.commodity_name,
            sc.sub_commodity_name,
            cr.commodity_type,
            1 is_contact

            FROM company_role cr
            JOIN role rl ON rl.id = cr.role_id
            JOIN sub_commodity sc ON sc.id = cr.sub_commodity_id
            JOIN user_profile up ON up.user_id = cr.user_id
            JOIN company_profile cp ON cp.user_id = cr.user_id
            JOIN regency rg ON rg.id = cp.company_regency_id
            JOIN province pv ON pv.id = rg.province_id
            JOIN commodity cdy ON cdy.id = sc.commodity_id
            WHERE cr.sub_commodity_id = $subCommodityId
            AND pv.id = $provinceId
            AND cr.role_id = ".Role::TRADER;

        return \DB::select($query);
    }

    private static function filterTransporter($subCommodityId, $provinceId)
    {
        $query = "SELECT 
            cp.user_id,
            sc.commodity_id,
            cp.company_name,
            up.fullname,
            rg.regency_name,
            pv.province_name,
            rl.role_name,
            cdy.commodity_name,
            sc.sub_commodity_name,
            cr.commodity_type,
            1 is_contact

            FROM company_role cr
            JOIN role rl ON rl.id = cr.role_id
            JOIN sub_commodity sc ON sc.id = cr.sub_commodity_id
            JOIN user_profile up ON up.user_id = cr.user_id
            JOIN company_profile cp ON cp.user_id = cr.user_id
            JOIN regency rg ON rg.id = cp.company_regency_id
            JOIN province pv ON pv.id = rg.province_id
            JOIN commodity cdy ON cdy.id = sc.commodity_id
            WHERE cr.sub_commodity_id = $subCommodityId
            AND pv.id = $provinceId
            AND cr.role_id = ".Role::TRANSPORTER;

        return \DB::select($query);
    }

    private static function filterSurveyor($subCommodityId, $provinceId)
    {
        $query = "SELECT 
            cp.user_id,
            sc.commodity_id,
            cp.company_name,
            up.fullname,
            rg.regency_name,
            pv.province_name,
            rl.role_name,
            cdy.commodity_name,
            sc.sub_commodity_name,
            cr.commodity_type,
            1 is_contact

            FROM company_role cr
            JOIN role rl ON rl.id = cr.role_id
            JOIN sub_commodity sc ON sc.id = cr.sub_commodity_id
            JOIN user_profile up ON up.user_id = cr.user_id
            JOIN company_profile cp ON cp.user_id = cr.user_id
            JOIN regency rg ON rg.id = cp.company_regency_id
            JOIN province pv ON pv.id = rg.province_id
            JOIN commodity cdy ON cdy.id = sc.commodity_id
            WHERE cr.sub_commodity_id = $subCommodityId
            AND pv.id = $provinceId
            AND cr.role_id = ".Role::SURVEYOR;

        return \DB::select($query);
    }

    private static function filterPurchaser($subCommodityId, $provinceId)
    {
        $query = "SELECT 
            cp.user_id,
            sc.commodity_id,
            cp.company_name,
            up.fullname,
            rg.regency_name,
            pv.province_name,
            rl.role_name,
            cdy.commodity_name,
            sc.sub_commodity_name,
            cr.commodity_type,
            1 is_contact

            FROM company_role cr
            JOIN role rl ON rl.id = cr.role_id
            JOIN sub_commodity sc ON sc.id = cr.sub_commodity_id
            JOIN user_profile up ON up.user_id = cr.user_id
            JOIN company_profile cp ON cp.user_id = cr.user_id
            JOIN regency rg ON rg.id = cp.company_regency_id
            JOIN province pv ON pv.id = rg.province_id
            JOIN commodity cdy ON cdy.id = sc.commodity_id
            WHERE cr.sub_commodity_id = $subCommodityId
            AND pv.id = $provinceId
            AND cr.role_id = ".Role::PURCHASER;

        return \DB::select($query);
    }
}
