<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PostTag;

class PostTagController extends Controller
{
    public function index($postId)
    { 
        $tags = PostTag::where('post_id', $postId)->get();
        
        return response()->json([
            'status' => 200, 
            'tags' => $tags
        ]);
    }

    public function store(Request $request, $postId)
    { 
        $tag = new PostTag;

        $tag->post_id = $postId;
        $tag->tag = $request->input('tag');
        $tag->save();

        return response()->json([
            'status' => 201,
            'tag' => $tag
        ]); 
    }

    public function show($postId, $id)
    {
        $tag = PostTag::where('post_id', $postId)->find($id);
        
        return response()->json([
            'status' => 200,
            'tag' => $tag
        ]);
    }

    public function update(Request $request, $postId, $id)
    {
        $tag = PostTag::find($id);

        $tag->post_id = $postId;
        $tag->tag = $request->input('tag');
        $tag->save();

        return response()->json([
            'status' => 201,
            'tag' => $tag
        ]);
    }

    public function destroy($postId, $id)
    { 
        $tag = PostTag::find($id);

        $tag->delete();

        return response()->json([
            'status' => 201,
            'message' => 'delete success'
        ]);
    }
}
