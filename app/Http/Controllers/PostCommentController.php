<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PostComment;

class PostCommentController extends Controller
{
    public function index($postId)
    { 
        $comments = PostComment::where('post_id', $postId)->get();
        
        return response()->json([
            'status' => 200,
            'comments' => $comments
        ]);
    }

    public function store(Request $request, $postId)
    {
        $comment = new PostComment;

        $comment->post_id = $postId;
        $comment->comment = $request->input('comment');
        $comment->save();

        return response()->json([
            'status' => 201,
            'comment' => $comment
        ]); 
    }

    public function show($postId, $id)
    {
        $comment = PostComment::where('post_id', $postId)->
            find($id);
        
        return response()->json([
            'status' => 200,
            'comment' => $comment
        ]);
    }

    public function update(Request $request, $postId, $id)
    {
        $comment = PostComment::find($id);

        $comment->post_id = $postId;
        $comment->comment = $request->input('comment');
        $comment->save();

        return response()->json([
            'status' => 201,
            'comment' => $comment
        ]); 
    }

    public function destroy($id)
    {
        $comment = PostComment::find($id);

        $comment->delete();

        return response()->json([
            'status' => 201,
            'message' => 'delete success'
        ]);
    }
}
