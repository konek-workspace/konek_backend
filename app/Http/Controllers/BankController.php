<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Bank;

class BankController extends Controller
{
    public function index()
    { 
        $banks = Bank::all();

        return response()->json([
            'status' => 200,
            'banks' => $banks
        ]);
    }

    public function store(Request $request)
    {
        $bank = new Bank;

        $bank->bank_code = $request->input('bank_code');
        $bank->bank_name = $request->input('bank_name');
        $bank->save();

        return response()->json([
            'status' => 201,
            'bank' => $bank
        ]);
    }

    public function show($id)
    {
        $bank = Bank::find($id);

        return response()->json([
            'status' => 200,
            'bank' => $bank
        ]);
    }

    public function update(Request $request, $id)
    { 
        $bank = Bank::find($id);

        $bank->bank_code = $request->input('bank_code');
        $bank->bank_name = $request->input('bank_name');
        $bank->save();

        return response()->json([
            'status' => 201,
            'bank' => $bank
        ]);
    }

    public function destroy($id)
    { 
        $bank = Bank::find($id);

        $bank->delete();

        return response()->json([
            'status' => 201,
            'message' => 'delete success'
        ]);
    }
}
