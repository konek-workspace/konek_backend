<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Role;

class RoleController extends Controller
{
    public function index()
    {
        $roles = Role::all();

        return response()->json([
            'status' => 200,
            'roles' => $roles
        ]);
    }

    public function store(Request $request)
    { 
        $role = new Role;

        $role->role_name = $request->input('role_name');
        $role->role_desc = $request->input('role_desc');
        $role->role_status = $request->input('role_status');
        $role->save();

        return response()->json([
            'status' => 201,
            'role' => $role
        ]);
    }

    public function show($id)
    {
        $role = Role::find($id);
        
        return response()->json([
            'status' => 200,
            'role' => $role
        ]);
    }

    public function update(Request $request, $id)
    {
        $role = Role::find($id);

        $role->role_name = $request->input('role_name');
        $role->role_desc = $request->input('role_desc');
        $role->role_status = $request->input('role_status');
        $role->save();

        return response()->json([
            'status' => 201,
            'role' => $role
        ]);
    }

    public function destroy($id)
    { 
        $role = Role::find($id);
        
        $role->delete();

        return response()->json([
            'status' => 201,
            'message' => 'delete success'
        ]);
    }
}