<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contact;

class ContactController extends Controller
{
    public function index()
    {
        return response()->json([
            'status' => 200
        ]);
    }

    public function store(Request $request)
    { 
        $contact = new Contact;

        $contact->user_id = $request->input('user_id');
        $contact->contact_id = $request->input('contact_id');

        $contact->save();

        return response()->json([
            'status' => 201,
            'contact' => $contact
        ]);
    }

    public function show($id)
    {
        $contacts = Contact::with(['contacts.userProfile', 
            'contacts.companyProfile'])->
            where('user_id', $id)->get();

        return response()->json([
            'status' => 200,
            'contacts' => $contacts
        ]);
    }

    public function update(Request $request, $id)
    {
        return response()->json([
            'status' => 201
        ]);
    }

    public function destroy($id)
    {
        $contact = Contact::find($id);

        $contact->delete();
        
        return response()->json([
            'status' => 201,
            'message' => 'delete success'
        ]);
    }
    
}
