<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserStoreRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'email' => 'required|unique:users|email',
            'password' => 'required',

            'nik' => 'required|max:16',
            'fullname' => 'required|max:255',
            'user_address' => 'required|max:255',
            'user_regency_id' => 'required|numeric',

            'company_name' => 'required',
            'company_address' => 'required|max:255',
            'company_regency_id' => 'required|numeric',
            'company_roles.*.sub_commodity_id' => 'required|numeric',
            'company_roles.*.role_id' => 'required|numeric'
        ];
    }
}
