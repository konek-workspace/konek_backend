<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyRole extends Model
{
    protected $table = 'company_role';
    protected $hidden = ['created_at', 'updated_at'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function producerDesc()
    {
        return $this->hasOne('App\ProducerDesc');
    }

    public function traderDesc()
    {
        return $this->hasOne('App\TraderDesc');
    }

    public function transporterDesc()
    {
        return $this->hasOne('App\TransporterDesc');
    }

    public function surveyorDesc()
    {
        return $this->hasOne('App\SurveyorDesc');
    }

    public function purchaserDesc()
    {
        return $this->hasOne('App\PurchaserDesc');
    }
}
