<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserProfile extends Model
{
    protected $table = 'user_profile';
    protected $primaryKey = 'user_id';

    protected $hidden = ['created_at', 'updated_at'];
    
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function companyProfile()
    {
        return $this->belongsTo('App\CompanyProfile', 'user_id', 'user_id');

        // or
        // return $this->hasOne('App\CompanyProfile', 'user_id', 'user_id');
    }

    public function companyRoles()
    {
        return $this->hasMany('App\CompanyRole', 'user_id', 'user_id');
    }

    public function regency()
    {
        return $this->belongsTo('App\Regency', 'user_regency_id');
    }

}
