<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = 'role';
    protected $hidden = ['created_at', 'updated_at'];

    public const PRODUCER    = 1;
    public const TRADER      = 2;
    public const TRANSPORTER = 3;
    public const SURVEYOR    = 4;
    public const PURCHASER   = 5;
}
