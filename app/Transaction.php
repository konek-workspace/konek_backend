<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $table = 'transaction';
    protected $hidden = ['created_at', 'updated_at'];
    
    public function sender()
    {
        return $this->belongsTo('App\User', 'sender_id', 'id');
    }

    public function receiver()
    {
        return $this->belongsTo('App\User', 'receiver_id', 'id');
    }
}
