<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchaserDesc extends Model
{
    protected $table = 'purchaser_desc';
    protected $hidden = ['created_at', 'updated_at'];

    public function companyRole()
    {
        return $this->belongsTo('App\CompanyRole');
    }

    public function timeUnit()
    {
        return $this->belongsTo('App\TimeUnit', 'seasonal_throughput_time_unit_id');
    }

    public function quantityUnit()
    {
        return $this->belongsTo('App\TimeUnit', 'seasonal_throughput_quantity_unit_id');
    }
}
