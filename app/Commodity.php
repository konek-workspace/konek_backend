<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Commodity extends Model
{
    protected $table = 'commodity';
    protected $hidden = ['created_at', 'updated_at'];
    
    public function subCommodities()
    {
        return $this->hasMany('App\SubCommodity');
    }

    public const PERTANIAN           = 1;
    public const PERKEBUNAN          = 2;
    public const KEHUTANAN           = 3;
    public const KELAUTAN_PERIKANAN  = 4;
    public const PETERNAKAN          = 5;
    public const PERTAMBANGAN_ENERGI = 6;
}
