<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostTag extends Model
{
    protected $table = 'post_tag';
    protected $hidden = ['created_at', 'updated_at'];
    protected $fillable = ['tag_name'];

    public function post()
    {
        return $this->belongsTo('App\Post');
    }
}
