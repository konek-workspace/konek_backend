<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
*/

/*
|---------------------------------------------
| GLOBAL ROUTES
|---------------------------------------------
|
*/
// login
Route::post('/login', 'UserController@login');

// filter regencies by province_id
Route::get('/regencies/filter-province/{id}', 'RegencyLookup@filterByProvinceId');

/*
|---------------------------------------------
| RESTFULL ROUTES
|---------------------------------------------
| These routes should place in auth middleware
|
*/
// Route::middleware(['auth:api'])->group(function () {
    Route::resource('users', 'UserController');
    Route::resource('user-profiles', 'UserProfileController');
    Route::resource('company-profiles', 'CompanyProfileController');
    Route::resource('contacts', 'ContactController');
    Route::resource('provinces', 'ProvinceController');
    Route::resource('regencies', 'RegencyController');
    Route::resource('commodities', 'CommodityController');
    Route::resource('sub-commodities', 'SubCommodityController');
    Route::resource('roles', 'RoleController');
    Route::resource('transactions', 'TransactionController');
    Route::resource('banks', 'BankController');
    Route::resource('posts', 'PostController');
    Route::resource('posts.comments', 'PostCommentController');
    Route::resource('posts.tags', 'PostTagController'); 
// });

/*
|---------------------------------------------
| USER ROUTES 
|---------------------------------------------
| These routes should place in auth middleware
|
*/
// Route::middleware(['auth:api'])->group(function () {
    // filter commodities via CommodityController
    // Route::get('/commodities/sub-commodity/{sub_commodity_id}/province/{province_id}/role/{role_id}', 'CommodityController@filter');
    
    // filter commodities by its id 
    Route::get('/commodities/{id}/companies/{user_id}', 'CommodityController@companies');
    Route::get('/commodities/{id}/sub-commodities', 'CommodityController@getSubCommodity');

    // filter commodities via SubCommodityController
    Route::get('/sub-commodities/{sub_commodity_id}/province/{province_id}/role/{role_id}', 'SubCommodityController@filter');
    // filter post by its subcommodity id
    Route::get('/sub-commodities/{sub_commodity_id}/get-posts', 'PostController@getPostBySubCommodity');

    // filter commodities via CompanyRoleController
    // Route::get('/company-role/filter/{sub_commodity_id}/{province_id}/{role_id}', 'CompanyRoleController@filter');

    // get user contacts
    Route::get('/users/{id}/get-contacts', 'UserController@getContacts');
    // get user profile
    Route::get('/users/{id}/get-profile', 'UserController@getProfile');
// });